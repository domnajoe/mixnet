# -*- coding: utf-8 -*-
import pickle
import msgpack
from random import randint


def split_address(msg):
    ret_ids = []
    for i, p in enumerate(msg):
        if p:
            ret_ids.append(p)
        else:
            break
    return ret_ids, msg[i + 1:]


def serialize_object(obj, protocol=-1):
    p = pickle.dumps(obj, protocol)
    return msgpack.packb(p)


def deserialize_object(msg):
    p = msgpack.unpackb(msg)
    return pickle.loads(p)


def split_list(a_list):
    split_index = randint(0, len(a_list))
    return a_list[:split_index], a_list[split_index:]
