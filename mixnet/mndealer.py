# -*- coding: utf-8 -*-
import msgpack
import random
import zmq
from zmq.eventloop.zmqstream import ZMQStream
from util import split_address, serialize_object, deserialize_object
from config import *
from mncrypto import keygen, encrypt, decrypt, re_encrypt
from mnsharing import make_random_shares, recover_secret


class MNDealer(object):

    def __init__(self, context, main_ep):
        self.service_q = ServiceQueue
        socket = context.socket(zmq.ROUTER)
        socket.bind(main_ep)
        self.main_stream = ZMQStream(socket)
        self.main_stream.on_recv(self.on_message)
        self.client_stream = self.main_stream
        self._workers = {}
        self._workers_info = {}
        self._services = {}
        self._worker_cmds = {MSG_READY: self.on_ready,
                             MSG_REPLY: self.on_reply,
                             MSG_VOTES: self.on_votes,
                             MSG_RVOTES: self.on_rvotes,
                             MSG_FVOTES: self.on_fvotes,
                             }
        p, g, x, y = keygen(120, 1)
        self.p = p
        self.keys = {'g': g, 'x': x, 'y': y}
        self.shares = make_random_shares(p, minimum=MINIMUM, shares=SHARES)
        self.final_votes = []
        self.firstshuffle_workers = []
        self.secondshuffle_workers = []
        self.workers_num = 0
        print("Dealer: Dealer opened!")
        return

    def distribute_keys(self, wid):
        # TODO: dont send x
        random.shuffle(self.shares)
        tmp_shares = self.shares[:MINIMUM]
        to_send = [wid, b'', WORKER_PROTO, MSG_KEYS, serialize_object(tmp_shares), serialize_object(self.keys)]
        if self.main_stream.closed():
            self.shutdown()
        self.main_stream.send_multipart(to_send)

    def register_worker(self, wid, service):
        if wid in self._workers:
            return
        self._workers[wid] = WorkerTracker(WORKER_PROTO, wid, service, self.main_stream)
        # If service exists then add this worker to its workers queue, if not create it.
        if service in self._services:
            wq, wr = self._services[service]
            wq.put(wid)
        else:
            q = self.service_q()
            q.put(wid)
            self._services[service] = (q, [])
        self.firstshuffle_workers.append(wid)
        self.secondshuffle_workers.append(wid)
        self.workers_num += 1
        print("Dealer: New worker registered with id: '%s'." % wid)
        return

    def unregister_worker(self, wid):
        try:
            wtracker = self._workers[wid]
        except KeyError:
            # not registered, ignore
            return
        wtracker.shutdown()
        service = wtracker.service
        if service in self._services:
            wq, wr = self._services[service]
            wq.remove(wid)
        del self._workers[wid]
        print("Worker with id: '%s' was removed from the pool." % wid)
        return

    def shutdown(self):
        if self.client_stream == self.main_stream:
            self.client_stream = None
        self.main_stream.on_recv(None)
        self.main_stream.socket.setsockopt(zmq.LINGER, 0)
        self.main_stream.socket.close()
        self.main_stream.close()
        self.main_stream = None
        self._workers = {}
        self._services = {}
        print("MixNet shutting down!")
        return

    def on_ready(self, rp, msg):
        ret_id = rp[0]
        service = msg.pop(0)
        self.register_worker(ret_id, service)
        self.distribute_keys(ret_id)
        return

    def on_reply(self, rp, msg):
        ret_id = rp[0]
        wtracker = self._workers.get(ret_id)
        if not wtracker:
            # worker not found, ignore message
            return
        service = wtracker.service
        try:
            wq, wr = self._services[service]
            cp, msg = split_address(msg)
            cmd = msg.pop(0)

            # TODO: do something with msg

            wq.put(wtracker.id)
            if wr:
                proto, rp, msg = wr.pop(0)
                # TODO: do something with msg
        except KeyError:
            # unknown service
            print("Dealer: Worker with id: '%s' reports an unknown service." % ret_id)
        return

    def on_votes(self, rp, msg):
        # select a random worker and send him the votes
        print("Dealer: received new encrypted votes from %s --> %s" % (rp[0], self.decrypt(deserialize_object(msg[0]))))
        wq, wr = self._services[SERVICE_ECHO]
        random.shuffle(self.firstshuffle_workers)
        wid = self.firstshuffle_workers.pop(0)
        to_send = [wid, b'', WORKER_PROTO, MSG_VOTES, msg.pop(0)]
        if self.main_stream.closed():
            self.shutdown()
        self.main_stream.send_multipart(to_send)
        return

    def on_rvotes(self, rp, msg):
        # select a random worker and send him the votes
        print("Dealer: received new re-encrypted random votes from %s --> %s" % (rp[0], self.decrypt(deserialize_object(msg[0]))))
        wq, wr = self._services[SERVICE_ECHO]
        random.shuffle(self.secondshuffle_workers)
        wid = self.secondshuffle_workers.pop(0)
        to_send = [wid, b'', WORKER_PROTO, MSG_RVOTES, msg.pop(0)]
        if self.main_stream.closed():
            self.shutdown()
        self.main_stream.send_multipart(to_send)
        return

    def on_fvotes(self, rp, msg):
        # select a random worker and send him the votes
        self.final_votes = self.final_votes + self.decrypt(deserialize_object(msg.pop(0)))
        # print("Dealer: received new final votes from %s --> %s" % (rp[0], self.final_votes))
        self.workers_num -= 1

        if self.workers_num == 0:
            print("Dealer: received final votes %s" % self.final_votes)
            self.shutdown()
        return

    def decrypt(self, votes):
        return [decrypt(e, self.keys['x'], self.p)[0] for e in votes]

    def on_worker(self, proto, rp, msg):
        # print("Received a new reply from worker: %s." % rp)
        cmd = msg.pop(0)
        if cmd in self._worker_cmds:
            fnc = self._worker_cmds[cmd]
            fnc(rp, msg)
        else:
            # ignore unknown command
            pass
        return

    def on_message(self, msg):
        # print("Received: %s" % msg)
        rp, msg = split_address(msg)
        # dispatch on first frame after path
        t = msg.pop(0)
        if t.startswith(b'MNPW'):
            self.on_worker(t, rp, msg)
        else:
            print('Dealer: Unknown Protocol: "%s"' % t)
        return

    def find_worker(self, wid, service):
        wq, wr = self._services[service]
        if wq.__contains__(wid):
            wq.remove(wid)
            return wid
        else:
            return None


class WorkerTracker(object):
    def __init__(self, proto, wid, service, stream):
        self.proto = proto
        self.id = wid
        self.service = service
        self.stream = stream
        self.data = []
        return

    def set_stream(self, stream):
        self.stream = stream

    def shutdown(self):
        self.stream = None
        return


class ServiceQueue(object):
    def __init__(self):
        self.q = []
        return

    def __contains__(self, wid):
        return wid in self.q

    def __len__(self):
        return len(self.q)

    def remove(self, wid):
        try:
            self.q.remove(wid)
        except ValueError:
            pass
        return

    def put(self, wid):
        if wid not in self.q:
            self.q.append(wid)
        return

    def get(self):
        if not self.q:
            return None
        return self.q.pop(0)
