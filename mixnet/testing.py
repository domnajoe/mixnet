# -*- coding: utf-8 -*-
from mncrypto import keygen
from mnsharing import make_random_shares
from config import *
from mnvoting import randomly_initialize_votelist
from mnsharing import recover_secret
from mncrypto import encrypt, re_encrypt, decrypt


vote_list = randomly_initialize_votelist()
print("vote list is %s" % vote_list)

p, g, x, y = keygen(120, 1)

shares = make_random_shares(p, minimum=MINIMUM, shares=SHARES)
print("shares are %s" % shares[:MINIMUM])


print("p is %s" % p)
new_p = recover_secret(shares[:MINIMUM])

print("new_p is %s" % new_p)

vote_list = [encrypt(new_p, g, y, e) for e in vote_list]

vote_list = [re_encrypt(new_p, e) for e in vote_list]

final_votes = [decrypt(e, x, p)[0] for e in vote_list]
print("final vote list is %s" % final_votes)