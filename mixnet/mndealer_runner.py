# -*- coding: utf-8 -*-
import zmq
import doctest
from zmq.eventloop.ioloop import IOLoop
from mndealer import MNDealer


class MixNetRunner(MNDealer):
    pass


def run(address):
    context = zmq.Context()
    broker = MixNetRunner(context, address)
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        print("Interrupt received, stopping.")
    finally:
        # clean up
        broker.shutdown()
        context.term()


if __name__ == '__main__':
    doctest.testmod()
