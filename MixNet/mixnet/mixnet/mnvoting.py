import random


def randomly_initialize_votelist(l_length=2, l_options=10):
    v_range = range(0, l_options)
    return [e for e in random.sample(v_range, l_length)]
