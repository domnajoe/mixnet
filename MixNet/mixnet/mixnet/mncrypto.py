from __future__ import print_function
import random
from Crypto.Util import *


def egcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        g, y, x = egcd(b % a, a)
        return g, x - (b // a) * y, y


def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m


def generatePrimeAndGenerator(k, t):
    prime = number.getPrime(k)
    g = random.randint(2, prime - 1)
    return prime, g


def keygen(k, t):
    p, g = generatePrimeAndGenerator(k, t)
    x = random.randint(1, p - 1)
    h = pow(g, x, p)
    return p, g, x, h


def rand(p):
    return random.randint(1, p - 1)


def encrypt(p, g, y, m):
    # Encryption
    k0, k1 = rand(p), rand(p)  # r = (k0, k1)
    alpha0 = (m * pow(y, k0, p)) % p
    beta0 = pow(g, k0, p)
    alpha1 = pow(y, k1, p)
    beta1 = pow(g, k1, p)
    ct = [(alpha0, beta0), (alpha1, beta1)]
    return ct


def decrypt(ct, x, p):
    # Decryption
    m0 = (ct[0][0] * modinv(pow(ct[0][1], x, p), p)) % p
    m1 = (ct[1][0] * modinv(pow(ct[1][1], x, p), p)) % p
    return m0, m1


def re_encrypt(p, ct):
    # Re-encryption (only source of randomness is r' = (k0', k1'))
    k0p, k1p = rand(p), rand(p)
    alpha0 = (ct[0][0] * pow(ct[1][0], k0p, p)) % p
    beta0 = (ct[0][1] * pow(ct[1][1], k0p, p)) % p
    alpha1 = pow(ct[1][0], k1p, p)
    beta1 = pow(ct[1][1], k1p, p)
    ct = [(alpha0, beta0), (alpha1, beta1)]
    return ct


if __name__ == "__main__":
    p, g, x, y = keygen(256, 1)

    # Generate a random message
    m = rand(p)

    ct = encrypt(p, g, y, m)

    m0, m1 = decrypt(ct, x, p)

    assert m1 == 1  # condition for decryption
    print("Plaintext             %x" % m)
    print("Decrypted message #1: %x" % m0)

    ctp = re_encrypt(p, ct)

    m0p, m1p = decrypt(ctp, x, p)

    assert m1p == 1
    print("Decrypted message #2: %x" % m0p)
