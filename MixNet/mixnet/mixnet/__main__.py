#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from argparse import ArgumentParser
from multiprocessing import Process

from mndealer_runner import run as dr
from mnworker_runner import run as wr


if __name__ == "__main__":
    parser = ArgumentParser(description='MixNet')
    parser.add_argument("m", help="running mode", type=str, default='dealer', choices=['dealer', 'voter', 'worker'])

    parser.add_argument("-p", "--port", type=int, default=5555, help="port address for the main endpoint")
    parser.add_argument("-a", "--address", type=str, default='127.0.0.1', help="main broker endpoint")

    parser.add_argument("-s", action="count", default=0, help="The number of nodes we need to activate, -s for "
                                                              "one -ss for two and so on")
    args = parser.parse_args()

    try:
        main_endpoint = 'tcp://%s:%s' % (args.address, args.port)

        items = args.s

        if args.m == 'dealer':
            # dr(main_endpoint)
            Process(target=dr, args=(main_endpoint,)).start()
            for x in range(0, items):
                Process(target=wr, args=(main_endpoint,)).start()
        elif args.m == 'worker':
            # wr(main_endpoint)
            for x in range(0, items):
                Process(target=wr, args=(main_endpoint,)).start()
        else:
            pass
    except KeyboardInterrupt:
        pass
