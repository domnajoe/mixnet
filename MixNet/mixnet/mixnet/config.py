# GENERAL CONFIGURATION
VOTER_PROTO = b'MNPV01'  #: Client protocol identifier
WORKER_PROTO = b'MNPW01'  #: Worker protocol identifier

# MSG TYPES
MSG_READY = b'\x01'
MSG_QUERY = b'\x02'
MSG_REPLY = b'\x03'
MSG_KEYS = b'\x04'
MSG_VOTES = b'\x05'
MSG_RVOTES = b'\x06'
MSG_FVOTES = b'\x07'

# SERVICES
SERVICE_ECHO = b'echo'

# CRYPTO
PRIME = 2 ** 127 - 1
MINIMUM = 3
SHARES = 6
