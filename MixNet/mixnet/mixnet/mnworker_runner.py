# -*- coding: utf-8 -*-
import msgpack
import doctest
import zmq
from zmq.eventloop.ioloop import IOLoop
from mnworker import MNWorker
from config import *

WK_SERVICE = SERVICE_ECHO


class WorkerRunner(MNWorker):
    HB_INTERVAL = 1000
    HB_RETRIES = 3

    def on_request(self, msg):
        cmd = msg.pop(0)
        res = []
        # TODO: do something with request
        print("Work done for operation: %s." % cmd)
        self.reply([cmd, msgpack.packb(res)])
        return


def run(address):
    context = zmq.Context()
    worker = WorkerRunner(context, address, WK_SERVICE)
    try:
        IOLoop.instance().start()
        worker.shutdown()
    except KeyboardInterrupt:
        print("Interrupt received, stopping!")
    finally:
        # clean up
        worker.shutdown()
        context.term()


if __name__ == '__main__':
    doctest.testmod()
