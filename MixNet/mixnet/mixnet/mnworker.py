# -*- coding: utf-8 -*-
import random
import zmq
from zmq.eventloop.zmqstream import ZMQStream
from zmq.eventloop.ioloop import IOLoop
from util import split_address, serialize_object, deserialize_object, split_list

from config import *
from mnvoting import randomly_initialize_votelist
from mnsharing import recover_secret
from mncrypto import encrypt, re_encrypt, decrypt


class ConnectionNotReadyError(RuntimeError):
    pass


class MissingHeartbeat(UserWarning):
    pass


class MNWorker(object):

    _proto_version = WORKER_PROTO  # worker protocol version

    def __init__(self, context, endpoint, service):
        self.context = context
        self.endpoint = endpoint
        self.service = service
        self.envelope = None
        self.stream = None
        self._tmo = None
        self.need_handshake = True
        self.connected = False
        self.vote_list = randomly_initialize_votelist()
        print("Worker: my initial votes are %s" % self.vote_list)
        self.received_key_shares = None
        self.received_keys = None
        self.p = None
        self._create_stream()
        print("Worker: Worker opened!")
        return

    def _create_stream(self):
        socket = self.context.socket(zmq.DEALER)
        ioloop = IOLoop.instance()
        self.stream = ZMQStream(socket, ioloop)
        self.stream.on_recv(self._on_message)
        self.stream.socket.setsockopt(zmq.LINGER, 0)
        self.stream.connect(self.endpoint)
        self._send_ready()
        return

    def _send_ready(self):
        ready_msg = [b'', WORKER_PROTO, MSG_READY, self.service]
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(ready_msg)
        return

    def shutdown(self):
        if not self.stream:
            return
        self.stream.socket.close()
        self.stream.close()
        self.stream = None
        self.need_handshake = True
        self.connected = False
        return

    def reply(self, msg):
        if self.need_handshake:
            raise ConnectionNotReadyError()
        to_send = self.envelope
        self.envelope = None
        if isinstance(msg, list):
            to_send.extend(msg)
        else:
            to_send.append(msg)
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(to_send)
        return

    def _on_message(self, msg):
        # 1st part is empty
        msg.pop(0)
        # 2nd part is protocol version
        proto = msg.pop(0)
        if proto != WORKER_PROTO:
            # ignore message from not supported protocol
            pass
        # 3rd part is message type
        msg_type = msg.pop(0)
        self.need_handshake = False
        if msg_type == MSG_QUERY:  # request
            # remaining parts are the user message
            print("Worker: Received new request: %s." % msg)
            envelope, msg = split_address(msg)
            envelope.append(b'')
            envelope = [b'', WORKER_PROTO, MSG_REPLY] + envelope  # reply
            self.envelope = envelope
            self.on_request(msg)
        elif msg_type == MSG_KEYS:  # receiving new key shares
            self.received_key_shares = deserialize_object(msg.pop(0))
            self.received_keys = deserialize_object(msg.pop(0))
            self.recover_secret()
            self.encrypt()
            self.send_votelist()
            # print("Worker: received new key shares and recovered secret.")
        elif msg_type == MSG_VOTES:  # receiving new votes
            received_votes = deserialize_object(msg.pop(0))
            self.vote_list = self.vote_list + received_votes
            self.re_encrypt()
            self.send_random_votes()
            # print("Worker: received new votes.")
        elif msg_type == MSG_RVOTES:  # receiving new random re-encrypted votes
            received_votes = deserialize_object(msg.pop(0))
            self.vote_list = self.vote_list + received_votes
            self.send_final_votes()
            # print("Worker: received new re-encrypted votes.")
        else:
            # invalid message
            # ignored
            pass
        return

    def on_request(self, msg):
        pass

    def recover_secret(self):
        self.p = recover_secret(self.received_key_shares)

    def encrypt(self):
        # print("Worker: encrypting vote list")
        self.vote_list = [encrypt(self.p, self.received_keys['g'], self.received_keys['y'], e) for e in self.vote_list]
        return

    def re_encrypt(self):
        # print("Worker: re-encrypting vote list")
        self.vote_list = [re_encrypt(self.p, e) for e in self.vote_list]
        return

    def send_votelist(self):
        msg = [b'', WORKER_PROTO, MSG_VOTES, serialize_object(self.vote_list)]
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(msg)
        self.vote_list = []
        # print("Worker: sent encrypted votes to Dealer!")

    def send_random_votes(self):
        random.shuffle(self.vote_list)
        to_send, self.vote_list = split_list(self.vote_list)
        msg = [b'', WORKER_PROTO, MSG_RVOTES, serialize_object(to_send)]
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(msg)
        # print("Worker: sent re-encrypted random votes to Dealer!")

    def send_final_votes(self):
        msg = [b'', WORKER_PROTO, MSG_FVOTES, serialize_object(self.vote_list)]
        if self.stream.closed():
            self.shutdown()
        self.stream.send_multipart(msg)
        self.vote_list = []
        # print("Worker: sent final vote list to Dealer!")